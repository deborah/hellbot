import re

from datetime import timedelta
from errbot import BotPlugin, re_botcmd
from random import sample, uniform
from time import asctime, mktime, gmtime, sleep

# Setup some default variables for the regexp parser
# regular expressions should be case insensitive & verbose
flags = re.I | re.X

# The regular expression for hell parsing
# Non-word characters (\W) are acting here as an overly-broad synonym for
# punctuation. If they're too greedy, we can write a punctuation class.
hellcmd = re.compile(
    r"""^(?:feed|send|put|da[mr]n|toss|smite|
         condemn|throw|kick|banish|drag)\s+             # hell-feeding verbs
         (?P<damned>.*?)                # the damned items. non-greedy!
         (?P<back>\s+back)?             # common case "put foo back in hell"
         \s+(?:(?:in)?to|at)\s+         # prepositions
         (?:heck|hell|the\ bad\ place)  # the Bad Place
         (?:\W?\s+                      # if more, after punctuation & space
            (?P<quals>.*\w)             # save later bits of the phrase
         )?
         \W*\s*$""", flags)             # ignore punctuation, space at EOL

admincmd = re.compile(
    r"""^hell(?:,)?\s+admin             # anything beginning `hell, list`
        ((?:\s+)(?P<verb>               # optional verb after whitespace
        help|tally|list|                # verbs w/o arguments
        (?:damn
        (?P<clear>\s+clear)?            # optionally clear hell first
        (?:\s+(?P<fn>[\S]*))?           # grab a filename
        )|                               # end of `damn` args.
        (?:record
        (?:\s+(?P<fn2>[\S]*))?           # grab a filename
        )                               # end of `record` args.
        ))?
        \s*$""", flags)

def emote(say_string, style='mm'):
    """
    Different chat clients emote differently. Standard IRC syntax
    is to open with "/me " but in Slack and Mattermost this is usually
    replaced by an API call, and can be easily emulated by formatting the
    string in italics.
    """
    if style == 'irc':
        return("/me {}".format(say_string))
    else:
        return("_{}_".format(say_string))


class Hell(BotPlugin):
    """
    Process the Hell commands:
    * Sending items to hell
    * Administrative commands
    """
    @re_botcmd(pattern=admincmd, prefixed=False)
    def hell_admin(self, msg, match, clear=False):
        """
        * `hell, admin help` or `hell, admin`
        * `hell, admin tally`
        * `hell, admin list`
        * `hell, admin record`
        * `hell, admin damn`
        """
        if not match:
            return
        verb = match.group('verb') or 'help'

        if verb == 'help':
            usage = (
                "```Usage: hell, admin tally  Report number of damned items.\n"
                "or     hell, admin list   Report all damned items.\n"
                "or     hell, admin damn   Feed a file of items to hell.\n"
                "or     hell, admin [help] Print this message```"
            )
            yield(usage)
        elif verb == 'list':
            try:
                yield("All items in Hell (debugging only)")
                yield("```")
                for i in self['items']:
                    datestr = asctime(gmtime(int(i[0])))
                    yield("{} ({}): {}".format(
                        i[0], datestr, i[1]
                    ))
            except KeyError:
                yield("Hell is Empty")
            yield("```")
        elif verb == 'tally':
            try:
                yield("I rule over {} damned souls.".format(
                    len(self['items'])
                ))
            except KeyError:
                yield("Hell is Empty")
        elif verb.startswith('damn'):
            # Given a filename in the format `datetime item`, with `datetime`
            # in seconds since the epoch, feed hell all the items in
            # that list. Optionally, empty out hell beforehand.  `Filename`
            # is on the server where hellbot is running.
            # Usage message:
            damnusage = (
                "```Usage: hell, admin damn [filename]       "
                "File contents to hell\n"
                "or     hell, admin damn clear [filename] "
                "Same, clearing hell 1st\n"
                "or     hell, admin damn                  "
                "Print this message```"
            )

            # parse arguments
            if not match.group('fn'):
                yield(damnusage)
                return

            # Instantiate the items list. Errbot persistence means we can't
            # edit the set in place.
            if match.group('clear'):
                items = set()
            else:
                try:
                    items = self['items']
                except KeyError:
                    # Instantiating an empty item list
                    items = set()

            filename = match.group('fn')
            try:
                with open(filename, 'r') as f:
                    for line in f:
                        # Only the first space is a delimiter
                        datestr, item = line.split(' ', 1)
                        # Strip off the trailing newline, & add the tuple
                        items.add((datestr, item.strip()))
            except (PermissionError, FileNotFoundError) as e:
                print("Couldn't open that file: {}.".format(e))
                return

            # preserve the new item list
            self['items'] = items

        elif verb.startswith('record'):
            # Write the contents of hell into a file in the format
            # `datetime item`, with `datetime`  in seconds since the epoch.
            # Usage message:
            recordusage = (
                "```Usage: hell, admin record [filename]       "
                "Record Hell's denizens in a file\n"
                "or     hell, admin record                  "
                "Print this message```"
            )

            # parse arguments
            if not match.group('fn2'):
                yield(recordusage)
                return

            filename = match.group('fn2')
            try:
                with open(filename, 'w') as f:
                    for datestr, item in self['items']:
                        f.write("{} {}\n".format(datestr, item))
            except (PermissionError, FileNotFoundError) as e:
                print("Couldn't open that file for writing: {}.".format(e))
                return
            yield("Wrote {} lines to {}".format(len(self['items']), filename))

    def harrow_hell(self, items):
        """
        Remove 0-2 random items from hell. Chances of items being removed
        increases as the size of hell contents does.

        Uses random.sample() to choose the element; set.pop() is supposed to
        be random but doesn't seem to be.
        """
        freed = list()
        numitems = len(items)

        # FIXME: the old hellbot used this algorithm to determine
        # ejection chance. Ridiculous, but did a pretty good job. There's
        # probably a  simple formula that would also have good odds
        # but for now let's just replicate the original algorithm.
        # rerun chance in the old algorithm is roughly numitems / 5
        if numitems <= 5:
            chance = 12.0
        elif 5 < numitems <= 10:
            chance = 25.0
        elif 10 < numitems <= 15:
            chance = 40.0
        elif 15 < numitems <= 15:
            chance = 65.0
        elif 20 < numitems <= 20:
            chance = 80.0
        elif 30 < numitems <= 30:
            chance = 82.0
        elif 45 < numitems <= 60:
            chance = 85.0
        else:
            chance = 95.0

        items_removed = 0
        if uniform(0, 100) < chance:
            items_removed += 1
        if uniform(0, 100) < numitems / 5:
            items_removed += 1

        for i in range(items_removed):
            item = sample(items, 1)[0]
            freed.append(item)
            items.remove(item)

        return(items, freed)

    @re_botcmd(pattern=hellcmd, prefixed=False)
    def hell_send(self, msg, match):
        """
        Recognizes commands to feed Hell:
            `[verb] [damned item] [to/into/at] [hell] [optional qualifier]`
        which will send `[damned item] [optional qualifier]` to Hell.
        """
        # Process the incoming query
        if not match or not match.group('damned'):
            return

        # Join the main and qualifier parts of the damned item
        damned_full = ' '.join(
            [match.group('damned'), match.group('quals') or '']
        ).strip()

        # To ease reading of the common use case, after a harrowing,
        # `put [expelled item] back in hell`
        if match.group('back'):
            damned_full += ' (returned to Hell)'

        # Errbot's persistence model presumes that you can't append
        # items already in persistence; you must restore them.
        try:
            items = self['items']
        except KeyError:
            items = set()

        # Remove an item from hell before you add the new one,
        # so as not to accidentally harrow the newest item
        items, freed = self.harrow_hell(items)

        # Add a tuple of (current time, damned item) to a set.
        # Having them in a set means set.pop() will choose a random
        # damned item to eject in self.harrow_hell().
        # Seconds from the epoch for consistency with legacy data.
        now = int(mktime(gmtime()))

        # Add the new item to the set, & replace the persisting set
        items.add((now, damned_full))
        self['items'] = items

        yield(emote("sneaks out a scaly hand and grabs {}".format(
            damned_full
        )))

        # say a different thing on second pass
        verb = "emits a sudden"
        if freed:
            for item in freed:
                # measure how long it's been in hell
                duration_secs = now - int(item[0])
                duration = timedelta(seconds=duration_secs)
                yield(emote(
                    (
                        "{} roar as it expels {}. "
                        "(Stayed in hell for {} hours.)"
                    ).format(verb, item[1], str(duration))
                ))
                # brief delay just for digestion time. om nom nom.
                if len(freed) > 1:
                    sleep(uniform(1, 3))
                    verb = "continues to"
