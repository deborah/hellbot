# Copyright 2018 Deborah Kaplan
#
# This file is part of HellBot
# Source code is available at <https://gitlab.com/deborah/hellbot>.
#
# HellBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Import local settings which shouldn't go in git
try:
    from settings_local import *
except ImportError:
    pass
