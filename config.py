# Copyright 2018 Deborah Kaplan
#
# This file is part of HellBot
# Source code is available at <https://gitlab.com/deborah/hellbot>.
#
# HellBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#############################################################################
#                                                                           #
#  This config file draws some values from a file called                    #
#  `config_local.py`. That file SHOULD NOT BE COMMITED TO A PUBLIC REPO.    #
#  Copy the file `config_local_changeme.py` to `config_local.py`, then      #
#  modify any value labelled "FIXME".                                       #
#                                                                           #
#  For a more complete set of options:                                      #
#  https://github.com/errbotio/errbot/blob/master/errbot/config-template.py #
#  A subset of likely use to HellBot users is described here.               #
#                                                                           #
#############################################################################

import logging
import os

# Import local settings which shouldn't go in git
try:
    from config_local import *
except ImportError:
    exit("Failure: Couldn't import local settings.")

CORE_PLUGINS = (
    'ACLs',
    'Backup',
    'ChatRoom',
    'CommandNotFoundFilter',
    'Flows',
    'Health',
    'Plugins',
    'TextCmds',
    'Utils',
    'VersionChecker',
    'Webserver',
    # 'Help' disabled by default because it conflicts with ox help
)
BACKEND = 'Mattermost'
# Also allowed include:
# 'Text'     - on the text console
# 'Graphic'  - in a GUI window
# 'Slack'    - see https://slack.com/
# 'XMPP'     - Jabber


# BOT_ROOTDIR is the directory this file lives in
BOT_ROOTDIR = os.path.realpath(__file__).rsplit(os.path.sep, 1)[0]

BOT_DATA_DIR = os.path.join(BOT_ROOTDIR, 'data')
if not os.path.isdir(BOT_DATA_DIR):
    # create an empty data directory
    os.mkdir(BOT_DATA_DIR)
BOT_EXTRA_PLUGIN_DIR = os.path.join(BOT_ROOTDIR, 'hell')
BOT_LOG_FILE = os.path.join(BOT_ROOTDIR, 'hellbot.log')
BOT_LOG_LEVEL = logging.DEBUG

# `git clone https://github.com/Vaelor/errbot-mattermost-backend.git`
# into this directory
BOT_EXTRA_BACKEND_DIR = os.path.join(BOT_ROOTDIR, 'errbot-mattermost-backend')
