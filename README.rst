##################
HellBot
##################

.. image:: https://gitlab.com/deborah/hellbot/wikis/uploads/ee2c95435b78078e5cdb1bced955684c/Fantastic_adventures_194202.jpg
    :align: right
    :alt: Doorway to HELL, by Frank Patton, with a blonde lady surprised by a skull-headed, horned, scaly devil in a flaming gateway.
    
Credit: `Fantastic Adventures, February 1942 <https://commons.wikimedia.org/wiki/File:Fantastic_adventures_194202.jpg>`_

************
Introduction
************

Send things to hell. Periodically, Hell will regurgitate something from hell. That's terrible. Why would you send things back, Hell?

A plug-in for `Errbot <http://errbot.io/en/latest/index.html>`_. *Never run the bot as root or any other privileged user*!

This is a Python rewrite for Errbot of the old Livejournal IRC hEllbot in Perl, which was rewritten for Dreamwidth, which was rewritten for II&F for Jabber/XMPP.

Features
========

#. Unicode-compliant
#. Can handle left-to-right and right-to-left text.
#. Can talk to shell, Mattermost, XMPP/Jabber, IRC, Slack, or anything else where there's an Errbot backend.

Assumptions
===========

* Server: the server running the chat protocol
* User: an instance of a user on the Server
* Team: A group of collaborating users. (This is Mattermost syntax, needed for errbot-mattermost-backend.)
* Channel: A single chat room on a Team.
* Hell: This bot.

This documentation is written assuming the User on the Server who runs the bot is called ``@hell``.  Hell can run as any logged in user. 

Hell's saved state is dependent on a database file on disk. If the bot is run from a different location without pointing to the previous instance of the persistence database, data *will not persist*.

Requirements
============

* Python 3
* `virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/latest/install.html>`_ isn't required, but will certainly make your life easier.


Configuring and installing
==========================

The instructions here assume a Mattermost chat backend and a standard II&F configuration. Alternate instructions will be added as needed.

Mattermost Setup
----------------

#. Login to Mattermost as the account that will be running the bot.
#. From the Main Menu (visually the hamburger menu in the top left, beside the login name) choose *Account Settings*.
#. From the left-hand navigation, choose *Security*.
#. From *Security*, choose *Edit* to the right of *Personal Access Tokens*. If you don't see *Personal Access Tokens*, you don't have permission to create them, and you need to contact in administrator for your team. (Tell the administrator they can add the ability by going to *System Console*, *Users*, then find the bot user and go to *Manage Roles*.)
#. Select *Create New Token*.
#. Give your token a meaningful description, such as "Bot code for Hellbot".
#. Copy and paste your token; you will need it when you get to the config section below, and you won't be able to get it again.


Installing
----------

#. Pick the user on the server who'll run the bot's Python code.  We suggest running this in a virtualenv.
#. Make a parent directory for the config and state information. We're assuming that you're running in ``~/.hellbot``.
#. Get this repository into that new directory.
#. Get the mattermost plugin repo into the Hellbot directory.
#. Install the pre-requsites.
#. Copy the Hellbot ``config_local_changeme.py`` to ``config_local.py``.
#. Copy the Hellbot ``config.py`` to ``config.py``.
#. Modify every instance of ``FIXME`` in ``config_local.py``.
#. Read over the settings in config.py to make sure they're right.

.. code:: bash

   mkvirtualenv -p python3 hellbot        # make a virtual environment set up for Python 3
   mkdir ~/.hellbot
   cd ~/.hellbot
   git clone git@gitlab.com:deborah/hellbot.git
   cd hellbot
   git clone git@github.com:deborahgu/errbot-mattermost-backend.git
   pip install -r requirements.txt
   pip install -r errbot-mattermost-backend/requirements.txt
   cp config_local_changeme.py config_local.py
   vim config_local.py

   # And when that's done:
   vim config.py

You can start the bot as a daemon by running ``errbot --daemon``, though you
may want to leave it in non-daemon mode until it's tested and working. You can
test it on the shell by running ``errbot -T``.

From within the chat process (eg. Mattermost), direct message the Ox user
``!restart`` to restart the dameon and load fresh code.

Troubleshooting
--------------

#. You can run errbot at the shell by running ``errbot -T`` from the directory containing the config files.  `Read more about this mode <http://errbot.io/en/latest/user_guide/plugin_development/development_environment.html#local-test-mode>`_.
#. You can restart the errbot from Mattermost by saying ``!restart`` in a private chat with @hell
